import AlertExample from './components/alert-example'
import { AlertProvider } from './providers/alert-provider'
import './App.css'

const App = () => (
  <AlertProvider>
    <div className="App">
      <AlertExample />
    </div>
  </AlertProvider>
)

export default App
