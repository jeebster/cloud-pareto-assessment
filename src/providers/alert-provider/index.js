import React, { useMemo, useReducer } from 'react'
import alertReducer from '../../reducers/alert-reducer'

const AlertContext = React.createContext()

const AlertProvider = (props) => {
  const [alertReducerState, alertReducerDispatch] = useReducer(alertReducer, {
    alerts: []
  })

  const memoizeContextValue = useMemo(() => ({
    alertReducerState, alertReducerDispatch
  }), [alertReducerState, alertReducerDispatch])

  return (
    <AlertContext.Provider
      value={memoizeContextValue}
    >
      {props.children}
    </AlertContext.Provider>
  )
}

export { AlertContext, AlertProvider }