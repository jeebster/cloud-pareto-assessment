import reducer from './index'

const initialState = () => ({ alerts: [] })

describe('Alert Reducer: initialization', () => {
  const alert = {
    alertTitle: 'warning alert',
    alertType: 'warning',
    link: 'http://some.fake.link',
    text: 'this is a warning alert',
  }

  it('should generate an identifier if the id property is not present', () => {
    const action = {
      type: 'add',
      payload: alert,
    }

    const result = reducer(initialState(), action)
    const { alerts } = result
    const [targetAlert] = alerts

    expect(targetAlert).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
      })
    )
  })
})

describe(`Alert Reducer: 'add'`, () => {
  const alert = {
    id: 1,
    alertTitle: 'warning alert',
    alertType: 'warning',
    link: 'http://some.fake.link',
    text: 'this is a warning alert',
  }

  it('should add the alert object argument to the alerts state array', () => {
    const action = {
      type: 'add',
      payload: alert,
    }

    const result = reducer(initialState(), action)

    expect(result).toEqual(
      expect.objectContaining({
        alerts: [
          expect.objectContaining(alert)
        ]
      })
    )
  })
})

describe(`Alert Reducer: remove`, () => {
  it('should remove the alert object argument from the alerts state array', () => {
    const action = {
      type: 'remove',
      payload: alert,
    }

    const state = {
      alerts: [alert]
    }

    const result = reducer(state, action)

    expect(result).toEqual(
      expect.objectContaining({
        alerts: []
      })
    )
  })
})

describe(`Alert Reducer: errors`, () => {
  it('should throw an error when no action payload is provided', () => {
    const action = {
      type: 'remove'
    }

    expect(
      () => reducer(initialState(), action)
    ).toThrow()
  })

  it('should throw an error when an unsupported action type is provided', () => {
    const action = {
      type: 'noop',
      payload: alert,
    }

    expect(
      () => reducer(initialState(), action)
    ).toThrow()
  })
})