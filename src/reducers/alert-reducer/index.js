 // NOTE: A more robust strategy to generate UUID would be used in a production environment. This timestamp is for demonstration purposes only,
const generateId = () => Date.now()

const reducer = (state, action) => {
  const { type, payload } = action

  // NOTE: there should be more robust type checking and error handling in production,
  // this is for demonstration purposes.
  if (!payload) {
    throw new Error(`an alert object must be provided as the action payload`)
  }

  const { alerts } = state

  if (!payload.id) {
    payload.id = generateId()
  }

  switch (type) {
    case 'add':
      return {
        alerts: [...alerts, payload],
      }
    case 'remove':
      return {
        alerts: alerts.filter(alert => alert.id !== payload.id),
      }
    default:
      throw new Error(`action type must be specified in: ['add', 'remove']`)
  }
}

export default reducer