import { useEffect, useContext } from 'react'
import Alert from '../alert'
import { AlertContext } from '../../providers/alert-provider'

const AlertManager = () => {
  const { alertReducerState, alertReducerDispatch } = useContext(AlertContext)
  const { alerts } = alertReducerState

  useEffect(() => {
    alerts.forEach(alert => {
      setTimeout(() => {
        alertReducerDispatch({
          type: 'remove',
          payload: alert
        })
      }, alert.timeLimit)
    })
  }, [alerts, alertReducerDispatch])

  return (
    <ul>
      {alerts.map(alert => <Alert key={alert.id} {...alert}/>)}
    </ul>
  )
}

export default AlertManager