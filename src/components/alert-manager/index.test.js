import React from 'react'
import renderer from 'react-test-renderer'
import AlertManager from './index'

it('renders', () => {
  const tree = renderer
    .create(<AlertManager />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})