import PropTypes from 'prop-types'
import MUILink from '@material-ui/core/Link'
import MUIAlert from '@material-ui/lab/Alert'
import MUIAlertTitle from '@material-ui/lab/AlertTitle'

const Alert = ({ id, alertTitle, alertType, link, text}) => {
  const renderAlertTitle = () => (
    alertTitle ? <MUIAlertTitle>{alertTitle}</MUIAlertTitle> : null
  )

  const renderAlert = () => (
    <MUIAlert
      id={id}
      severity={alertType}
    >
      {renderAlertTitle(alertTitle)}
      {text}
    </MUIAlert>
  )

  return link ? <MUILink href={link}>{renderAlert()}</MUILink> : renderAlert()
}

Alert.propTypes = {
  id: PropTypes.number.isRequired,
  alertTitle: PropTypes.string,
  alertType: PropTypes.oneOf([
    'error',
    'warning',
    'info',
    'success',
  ]).isRequired,
  link: PropTypes.string,
  text: PropTypes.string.isRequired,
}

export default Alert