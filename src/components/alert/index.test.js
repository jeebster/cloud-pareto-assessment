import React from 'react'
import renderer from 'react-test-renderer'
import Alert from './index'

const props = {
  id: 1,
  alertTitle: 'warning alert',
  alertType: 'warning',
  link: 'http://some.fake.link',
  text: 'this is a warning alert',
}

it('renders', () => {
  const tree = renderer
    .create(<Alert {...props} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})