import React from 'react'
import renderer from 'react-test-renderer'
import AlertExample from './index'

it('renders', () => {
  const tree = renderer
    .create(<AlertExample />)
    .toJSON();
  expect(tree).toMatchSnapshot();
})