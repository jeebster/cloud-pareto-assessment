import { useContext, useState } from 'react'
import AlertManager from '../alert-manager'
import { AlertContext } from '../../providers/alert-provider'

const AlertExample = () => {
  const { alertReducerState, alertReducerDispatch } = useContext(AlertContext)

  const [formData, setFormData] = useState({
    id: 1,
    alertType: 'error',
    alertTitle: '',
    timeLimit: 10000, // milliseconds
    text: '',
    link: ''
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    alertReducerDispatch({
      type: 'add',
      payload: formData,
    })
  }

  const renderForm = () => (
    <form onSubmit={(e) => handleSubmit(e) }>
      <div>
        <label>ID</label>
        <br />
        <input
          name="id"
          value={formData.id}
          type="number"
          min="0"
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            id: parseInt(e.target.value),
          }))}
        />
      </div>

      <div>
        <label>Error Type</label>
        <br />
        <select
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            alertType: e.target.value,
          }))}
          value={formData.alertType}
        >
          <option value="error">Error</option>
          <option value="warning">Warning</option>
          <option value="info">Info</option>
          <option value="success">Success</option>
        </select>
      </div>

      <div>
        <label>Alert Title</label>
        <br />
        <input
          name="alertTitle"
          value={formData.alertTitle}
          type="text"
          placeholder="Enter a title"
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            alertTitle: e.target.value,
          }))}
        />
      </div>

      <div>
        <label>Alert Text</label>
        <br />
        <input
          name="text"
          value={formData.text}
          type="text"
          placeholder="Enter some text"
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            text: e.target.value,
          }))}
        />
      </div>

      <div>
        <label>Link</label>
        <br />
        <input
          name="text"
          value={formData.link}
          type="text"
          placeholder="http://"
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            link: e.target.value,
          }))}
        />
      </div>

      <div>
        <label>Time Limit (milliseconds)</label>
        <br />
        <input
          name="timeLimit"
          value={formData.timeLimit}
          type="number"
          min="0"
          onChange={(e) => setFormData(prevState => ({
            ...prevState,
            timeLimit: parseInt(e.target.value),
          }))}
        />
      </div>

      <div>
        <button>Submit</button>
      </div>
    </form>
  )

  return (
    <div>
      <div>
        <AlertManager {...alertReducerState} />
      </div>
      <div>
        <h1>Submit an Alert</h1>
        {renderForm()}
      </div>
    </div>
  )
}

export default AlertExample;